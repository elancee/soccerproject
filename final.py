import pandas as pd
"""Analyze the data on soccer players and determine who is the best."""

def read_players20(path):
    """Reads the dataset of soccer players and their stats.
    
    Args: 
        path (str): a path to a file
        
    Returns:
        (df): dataframe of data
    """
    data = pd.read_csv(path)
    names = data[["long_name","pace","height_cm","weight_kg",
                   "player_positions", "shooting" , "preferred_foot", "passing",
                    "skill_moves", "dribbling", "defending"]] 
    return names

class UltimatePlayers:
    
    def __init__(self, data):
        self.data = data

    def height_based_on_position(self, position):
        """Calculate the tallest player for each position.
        
        Args: 
            position (str): position of the player 

        Return:
            (str): return the tallest players height
        """
        pos = self.data[self.data.player_positions == position]
        pos = pos.sort_values("height_cm", ascending = False)
        player = pos.iloc[0].loc["long_name"]
        height_of = pos.iloc[0].loc["height_cm"]
        print(f"The tallest player that plays {position} is {player},"
                f"they are {height_of} tall.")
        return height_of
    
    
    def weight_based_on_height(self, height):
        """Calculate the biggest, the tallest and largest.
        
        Args: 
            height (int): height of the player

        Return:
            (str): return the largest player                
        """ 
        size = self.data[self.data.height_cm == height]
        size = size.sort_values("weight_kg", ascending = False)
        largest = size.iloc[0].loc["long_name"]
        return f"The largest player that is {height}cm tall is {largest}."
    
    def height_in_groups(self, height):
        """Creates a dataframe based on players height.
        
        Args:
            height(str): the height of the player
            
        Returns:
            (dataframe): returns the dataframe based on specified height
        """
        
        shortest = self.data[self.data.height_cm < 170]
        middle = self.data[self.data.height_cm <= 185]
        middle = middle[middle.height_cm >= 170]
        tallest = self.data[self.data.height_cm > 185 ]
        
        if height == "Shortest":
            return shortest, height
        elif height == "Average":
            return middle, height
        elif height == "Tallest":
            return tallest, height
             
    def pace_(self, height, category):
        """Calculates which player has the best pace depending on their height.

        Args:
            height(dataframe): dataframe of specific heights
            
        Returns:
            (str): Returns the fastest player
        """
        player = height.iloc[0].loc["long_name"]
        return f"Fastest player in the '{category}' players category"
                f" is {player}."
    
    def shooting_based_on_foot(self):
        """Calculates overall best shooter based on shooting ability and their
         preferred foot.

        Returns:
        (str): Returns the player with best shooting
        """
        left = self.data[self.data.preferred_foot == "Left"]
        right = self.data[self.data.preferred_foot == "Right"]
        left = left.sort_values("shooting" , ascending = False)
        right = right.sort_values("shooting", ascending = False)
        l = left.iloc[0].loc["long_name"]
        r = right.iloc[0].loc["long_name"]
        return f"The best left footed shooter is '{l}' and the best right "
            f"footed shooter is '{r}'."
    
    def passing_(self, height):
        """Calculates which player is the best at passing.

        Returns:
        (str): Returns the best passer and their passing score
        """
        top = (self.data["height_cm"] == height)
        top = self.data.sort_values("passing", ascending = False)
        pas = top.iloc[0].loc["long_name"]
        score = top.iloc[0].loc["passing"]
        return f"The best passer that is {height} is '{pas}' with a "
                f"passing score of {score}."
    
    def dribbling_(self, position):
        """Calculates overall best dribbler based on dribbling ability.

        Returns:
            (str): Returns the player with best dribbling skills
        """
        pos2 = self.data[self.data.player_positions == position]
        pos2 = pos2.sort_values("dribbling", ascending = False)
        dribbler = pos2.iloc[0].loc["long_name"]
        dribble = self.data.sort_values("dribbling", ascending = False)
        d = dribble.iloc[0].loc["long_name"]
        return f"The best dribbler that plays {position} is '{d}'."
      
        
    def defending_(self, position):
        """Calculates overall best defender based on defending ability and 
        their position.

        Args: 
            position (str): position of the player

        Returns:
            (str): Returns the player with best defended 
        """
        pos = self.data[self.data.player_positions == position]
        pos = pos.sort_values("defending", ascending = False)
        defender = pos.iloc[0].loc["long_name"]
        return f"The best defender that player plays {position} "
                f"is '{defender}'."


        
def main(data):   
    players = UltimatePlayers(data)
    
    user = int(input("To find out the tallest and biggest player based on their"
                     " position enter 1\n"
    "To find out the fastest player based on their height enter 2\n"
    "To find out who the best left footed and right footed shooters are enter"
    " 3\n" 
    "To find out who the best passer is based on their height enter 4\n"
    "To find out who the best dribbler is based on their position enter 5\n"
    "To find out who has the best defending skills based on position enter 6:"))
    if user == 1:
        pos = input("Enter a players position, use capital leters: ")
        height = players.height_based_on_position(pos)
        print(players.weight_based_on_height(height))
    elif user == 2:
        ht = input("Enter a height (Shortest, Average, Tallest): ")
        height, cat = players.height_in_groups(ht)
        print(players.pace_(height, cat))
    elif user == 3:
        print(players.shooting_based_on_foot())
    elif user == 4:
        ht = int(input("Enter a height, in cm: "))
        print(players.passing_(ht))
    elif user == 5:
        pos = input("Enter a players position, use capital leters: ")
        print(players.dribbling_(pos))
    elif user == 6:
        pos = input("Enter a players position, use capital leters: ")
        print(players.defending_(pos))
    else:
        print("That was an invalid choice.")

if __name__ == '__main__':
    data = read_players20("players_17.csv")
    main(data)
    
