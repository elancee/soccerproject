import unittest
import final

class TestUltimatesoccerteam(unittest.TestCase):
    
    def setUp(self):
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        d2019 = final.read_players20("players_19.csv")
        self.p2017 = final.UltimatePlayers(d2017)
        self.p2018 = final.UltimatePlayers(d2018)
        self.p2019 = final.UltimatePlayers(d2019)
               
    def test_init(self):
        """Test the UltimatePlayers.__init__ gives us an UltimatePlayers
        instance with the right dataframe."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        self.assertIsInstance(players17, final.UltimatePlayers)
            
    def test_pace(self):
        """Make sure that the pace function is returning the 
        correct player with the fastest pace."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.pace_(190, "Shortest"), 
                         "Fastest player in the 'Shortest' players category"
                         " is Alexis Alejandro Sánchez Sánchez.")
        self.assertEqual(player18.pace_(180, "Tallest"), "Fastest player in"
                         " the 'Tallest' players category is Manuel Neuer.")
    
    def test_shooting(self):
        """Make sure that the shooting function is returning the 
        correct player with the best shooting ability."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.shooting_based_on_foot(), 
                         "The best left footed shooter is 'Lionel Andrés "
                         "Messi Cuccittini' and the best right footed shooter"
                         " is 'Cristiano Ronaldo dos Santos Aveiro'.")
        self.assertEqual(players18.shooting_based_on_foot(), "The best left"
        " footed shooter is 'Lionel Andrés Messi Cuccittini' and the best right" 
        " footed shooter is 'Cristiano Ronaldo dos Santos Aveiro'.")
    
    def test_passing(self):
        """Make sure that the passing function is returning the 
        correct player with the best passing."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.passing_(170), 
                         "The best passer that is 170 is 'Andrea Pirlo' with "
                         "a passing score of 91.0.")
        self.assertEqual(players18.passing_(180), "The best passer that is "
                         "180 is 'Toni Kroos' with a passing score of 89.0.")
        
    def test_dribbling(self):
        """Make sure that the dribbling function is returning the 
        correct player with the best dribbling skill."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.dribbling_("ST"), 
                         "The best dribbler that plays ST is 'Lionel Andrés"
                         " Messi Cuccittini'.")
        self.assertEqual(players18.dribbling_("LW"), "The best dribbler that "
                         "plays LW is 'Lionel Andrés Messi Cuccittini'.")
        
    def test_defending(self):
        """Make sure that the defending function is returning the 
        correct player with the best defending skills."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.defending_("CB"), 
                         "The best defender that player plays CB is 'Jérôme"
                         " Boateng'.")
        self.assertEqual(players18.defending_("CM"), "The best defender that "
                         "player plays CM is 'Allan Marques Loureiro'.")
        
    def test_height_based_on_position(self):
        """This test Player.height is correct"""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.height_based_on_position("RW"), 190)
        self.assertEqual(players18.height_based_on_position("CB"), 201)
    
    def test_weight(self):
        """Test that Player.weight is correct."""
        d2017 = final.read_players20("players_17.csv")
        d2018 = final.read_players20("players_18.csv")
        players17 = final.UltimatePlayers(d2017)
        players18 = final.UltimatePlayers(d2018)
        self.assertEqual(players17.weight_based_on_height(201), "The largest "
                         "player that is 201cm tall is Ryan Herman.")
        self.assertEqual(players18.weight_based_on_height(188), "The largest "
                         "player that is 188cm tall is Luis Felipe Carvalho da "
                         "Silva.")
    
if __name__ == '__main__':
    unittest.main()    